<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Level as Lvl;
use App\Level;

class LevelController extends Controller
{
    public function ApiLevel(Request $request)
    {
    	if($request->q){
    		$level = Level::where('name', 'like', '%' . $request->q .'%')->get();
    	}else {
    		$level = Level::all();
    	}
        return response()->json([ 'results' => $level],200);
    }
}
