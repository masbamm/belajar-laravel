<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Category as Ctr;
use App\Category;

class CategoryController extends Controller
{
    public function ApiCategory(Request $request) {
    
    if($request->q){
    	$category = Category::where('name', 'like', '%' . $request->q .'%')->get(); 
    } else { 

    		$category = Category::all(); 
    	}

        return response()->json([ 'results' => $category],200);
    
}

}