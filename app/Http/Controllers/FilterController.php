<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Course;
use App\Level;
use App\Category;

class FilterController extends Controller
{
	public function filter(Request $request)
    {

        $category_list = $request->get('category_list');
        $course = Course::all();

        if($category_list){
         $course = collect($course)->filter(function($row) use ($category_list) {return stristr($row['category_id'], $category_list); });
             }
        {
        return view('katalog',['course'=>$course]);
        }
    }


}
