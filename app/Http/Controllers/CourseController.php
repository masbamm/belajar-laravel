<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Course;
use App\Level;
use App\Category;






class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $keyword = $request->get('keyword');
        $category_list = $request->get('category_list');
        $level_list = $request->get('level_list');
        $course = Course::all();


        if(!empty($keyword)){
         $course = collect($course)->filter(function($row) use ($keyword) {return stristr($row['name'], $keyword); });
        }

        else if($category_list){
         $course = collect($course)->filter(function($row) use ($category_list) {return stristr($row['category_id'], $category_list); });
             }

         else if($level_list){
         $course = collect($course)->filter(function($row) use ($level_list) {return stristr($row['level_id'], $level_list); });
             }

     {
        return view('katalog',['course'=>$course]);
     }

    }






public function show(Course $course){


    return view('detail', compact('course'));

}



public function create(Course $course , Level $level , Category $category)

{

     $category = Category::all();
     $level = Level::all();

    return view('tambah',compact('course','level','category'));
}




public function store(Request $request)
{
    Course::create([
        'name' => $request->name,
        'desc' => $request->desc,
        'category_id' => $request->category_list,
        'level_id' => $request->level_list

    ]);

    return redirect ('katalog')->with('status','Data Kursus Berhasil Ditambahkan!');
}


public function destroy(Course $course)
{

    Course::destroy($course->id);

    return redirect ('katalog')->with('status','Data Kursus Berhasil Dihapus!');
}


public function edit(Course $course, Level $level , Category $category)
{

    $category = Category::all();
    $level = Level::all();

    return view ('edit',compact('course','level','category'));
}

public function update(Request $request, Course $course)
{
    Course::where('id',$course->id)
        ->update([
             'name' => $request->name,
             'desc' => $request->desc,
             'category_id' => $request->category_list,
             'level_id' => $request->level_list

        ]);

        return redirect ('katalog')->with('status','Data Kursus Berhasil Diubah!');
}




}