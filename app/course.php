<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "course";

    protected $fillable = ['name','desc','category_id','level_id'];
    
    public function level()
    {
    	return $this->belongsTo(Level::class);
    }

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
}
