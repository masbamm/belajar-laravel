@extends('layout/main')

@section('title','Detail Course')

@section('container')
<div class="container">
	<div class="row">
		<div class="col-10">
			<h1 class="mt-3">Detail Course</h1>
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Name</th>
						<th scope="col">Description</th>
						<th scope="col">Category</th>
						<th scope="col">Level</th>
						<th scope="col">Aksi</th>
					</tr>
				</thead>
				<tbody>
					


					<tr>
						<td>{{$course -> id}}</td>
						<td>{{$course -> name}}</td>
						<td>{{$course -> desc}}</td>
						<td>{{$course -> category->name }}</td>
						<td>{{$course -> level->name }}</td>
						<td>

							<a href="{{ $course->id }}/edit" class="btn btn-primary"> Edit </a>

							<form action="{{$course -> id}}" method="POST" class="d-inline"> 
								
								{{ method_field('delete') }}
								{{ csrf_field() }}
							<button type="submit" class="btn btn-danger">Delete</button>
							</form>
						</td>
					</tr>
					
					
				</tbody>
			</table>
		</tbody>
	</table>
</div>
</div>
</div>



@endsection