@extends('layout/main')

@section('title','Edit Data Kursus')

@section('container')

<div class="container">   
  <h1>Edit Data Kursus<br></h1> 
  <form class="form-horizontal" method="POST" action="/detail/{{ $course->id }}">
    {{ method_field('patch') }}
    {{ csrf_field() }}
    <div class="form-group">
      <label class="control-label col-sm-2 my-1 mt-4" for="name">Name :</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" placeholder="Masukan Nama Course" name="name" required value="{{ $course->name }}">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2 my-1 " for="desc">Description :</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="desc" placeholder="Deskripsi Kursus" name="desc" value="{{ $course->desc }}">
      </div>
    </div>


    

    <div class="col-sm-10">
      <label class="control-label" for="category_list">Category :</label>
      <select id="category_list" name="category_list" id="category_list" class="custom-select"></select>
    </div>



    <div class="col-sm-10">
      <label class="control-label my-3" for="category_list">Category :</label>
      <select id="level_list" name="level_list" id="level_list" class="custom-select"></select>
    </div>


    <div>
     <button type="submit" class="btn btn-primary ml-3 mt-3">Ubah Data</button>
   </form>   
 </div>



 @endsection

 @section('js')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
 <script type="text/javascript">


    
    $('#category_list').select2({

      placeholder: "Select category",
      closeOnSelect: true,
      ajax: {
          url: 'http://localhost:8001/api/categories',
          dataType: 'json',
          type: 'GET',
          data: function (params) {
          return {
            q: params.term, // search term
            page: params.page
          };
        },
          processResults: function (data,params) {
            return {
              results: $.map(data.results, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })              
            };
          },
          cache: true
      }
    });
    var data = {
    id: <?= $course -> category->id ?>,
    text: '<?= $course -> category->name ?>'
    };

    var newOption = new Option(data.text, data.id, false, false);
    $('#category_list').append(newOption).trigger('change');


$('#level_list').select2({
  placeholder: "Select Level",
  closeOnSelect: true,
  ajax: {
    url: 'http://localhost:8001/api/levels',
    dataType: 'json',
    type: 'GET',
    data: function (params) {
     return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
            return {
              results: $.map(data.results, function (item) {
                return {
                  text: item.name,
                  id: item.id
                }
              })              
            };
          },
          cache: true
        }
      });

    var data = {
    id: <?= $course -> level->id ?>,
    text: '<?= $course -> level->name ?>'
    };

    var newOption = new Option(data.text, data.id, false, false);
    $('#level_list').append(newOption).trigger('change');



    </script>
    @endsection
