@extends('layout/main')

@section('title','Pilih Course')

@section('container')


<div class="container">
	<div class="row">
		<div class="col-10">
			<h1 class="mt-3">Pilih Course</h1>
			<div class="col-xs-10">
			<a href="/tambah"  class="btn btn-primary btn-sm float-right my-2">Tambah Course</a>
			</div>


			 <div class="form-group col-7">
			 <form action="{{url("/katalog")}}" class="form-inline my-3" method="GET">
      		 <select value="{{Request::get('category_list')}}" name="category_list" id="category_list" class="col-sm-10">
      		 </select>
           <button class="btn btn-outline-success ml-2 " type="submit">Search</button>

           <form action="{{url("/katalog")}}" class="form-inline" method="GET">
            <select value="{{Request::get('level_list')}}" name="level_list" id="level_list" class="col-sm-10">
            </select>
            <button class="btn btn-outline-success ml-2 my-3" type="submit">Search</button>
      		 </div>
      	</form>



       {{-- <div class="form-group col-4">

      		 </div>
      	</form> --}}


			@if (session('status'))
				<div class="alert alert-info" role="alert">

					{{session('status')}}
				</div>
			@endif

			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Category</th>
						<th scope="col">Level</th>
						<th scope="col">Aksi</th>
					</tr>

				</thead>



				@foreach ($course as $key => $crs)


				<tr>
					<td>{{++$key}}</td>
					<td>{{$crs -> name}}</td>
					<td>{{$crs -> category->name}}</td>
					<td>{{$crs -> level->name}}</td>
					<td>
						<a href="/detail/{{$crs -> id}}" class="badge badge-success">Lihat Detail</a>

					</td>

				</tr>


				@endforeach
			</table>
		</div>
	</div>
</div>



@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $('#category_list').select2({
      placeholder: "Select category",
      closeOnSelect: true,
      ajax: {
          url: 'http://localhost:8001/api/categories',
          dataType: 'json',
          type: 'GET',
          data: function (params) {
          return {
            q: params.term, // search term
            page: params.page
          };
        },
          processResults: function (data,params) {
            params.page = params.page || 1;
            return {
              results: $.map(data.results, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
          },
          cache: true
      }
    });



     $('#level_list').select2({
      placeholder: "Select Level",
      closeOnSelect: true,
      ajax: {
          url: 'http://localhost:8001/api/levels',
          dataType: 'json',
          type: 'GET',
          data: function (params) {
           return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
              params.page = params.page || 1;
              return {
              results: $.map(data.results, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
          },
          cache: true
      }
    });

    </script>
@endsection