<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('depan');
// });

// Route::get('/katalog', function () {

//     return view('katalog');
// });


Route::get('/', 'Page@home');


Route::get('/tambah','CourseController@create');



 Route::get('/katalog', 'CourseController@index');
 





Route::get('/detail/{course}', 'CourseController@show');

Route::post('/tambah','CourseController@store');

Route::delete('/detail/{course}', 'CourseController@destroy');

Route::get('/detail/{course}/edit','CourseController@edit');

Route::get('/detail/{level}/edit','CourseController@level');


Route::patch('/detail/{course}','CourseController@update');


// Route::get('/katalog','FilterController@filter');


Route::group(['prefix' => 'api'], function(){
	Route::get('/categories','CategoryController@ApiCategory');
	Route::get('/levels','LevelController@ApiLevel');
});

















